import React from 'react';
import ToolbarCss from './Toolbar.module.css'

const toolbar = (props) => (
    <header className={ToolbarCss.Toolbar}>
        <div>menu</div>
        <div>logo</div>
        <nav>
            ...
        </nav>
    </header>
)

export default toolbar
