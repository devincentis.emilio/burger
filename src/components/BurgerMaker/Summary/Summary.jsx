import React from 'react';
import Button from './../../UI/Button/Button';

const summary = props => {
    const ingredientSummary = Object.keys(props.ingredients)
        .map(igKeys => {
            return (
                <li key={igKeys}>
                    <span style={{ textTransform: 'capitalize' }}> {igKeys} </span>: {props.ingredients[igKeys]}
                </li>
            )
        }
        )

    return (
        <>
            <h3> your order</h3>
            <p> ingredients list:</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p>totale: <strong>{props.price.toFixed(2)}</strong></p>
            <p>contiuna</p>
            <Button clicked={props.purchaseCanceled} BtnType='Danger'> CANCEL </Button>
            <Button clicked={props.purchaseContinued} BtnType='Success' > CONTINUE </Button>
        </>
    )
}

export default summary
