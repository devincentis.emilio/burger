import React from 'react';
import Ingredients from './ingredients/Ingredients';
import BurgerCss from './BurgerView.module.css'

const BurgerView = (props) => {
    let ingredientsTrasf = Object.keys(props.ingredients)
        .map(ingKey => {
            return (
                [...Array(props.ingredients[ingKey])].map((_, i) => {
                    return (
                        <Ingredients key={ingKey + '_' + Math.random().toString(36).substr(2, 9)} type={ingKey} />
                    )
                })
                    .reduce((arr, el) => arr.concat(el), [])
            )
        })
    if (ingredientsTrasf.length === 0) {
        ingredientsTrasf = <p>inserire</p>
    }
    return (
        <div className={BurgerCss.Burger}>
            <Ingredients type="bread-top" />
            {ingredientsTrasf}
            <Ingredients type="bread-Bottom" />
        </div>
    )
    
}

export default BurgerView