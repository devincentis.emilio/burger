import React from 'react';
import BurgerControlliCss from './BurgerControlli.module.css'
import ControllerBurger from './ControllerBurger/ControllerBurger';

const controlls = [
    { label: 'salad', type: 'salad' },
    { label: 'cheese', type: 'cheese' },
    { label: 'bacon', type: 'bacon' },
    { label: 'meat', type: 'meat' }
]

const BurgerControlli = (props) => (
    <div className={BurgerControlliCss.BurgerControlli}>
        <p>price: <strong>{props.price.toFixed(2)}</strong></p>
        {controlls.map(ctrl => <ControllerBurger key={ctrl.label} label={ctrl.label}
            added={() => props.IngredientsAdded(ctrl.type)}
            removed={() => props.IngredientsRemoved(ctrl.type)}
            disabled={props.disabled[ctrl.type]} />)}

        <button className={BurgerControlliCss.OrderButton}
            disabled={!props.purchasable}
            onClick={props.ordered}>order now</button>
            
    </div>
)

export default BurgerControlli