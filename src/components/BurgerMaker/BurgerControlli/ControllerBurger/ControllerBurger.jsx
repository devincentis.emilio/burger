import  React  from 'react';
import Classi from './ControllerBurger.module.css'

const ControllerBurger = (props) =>( 
    <div className={Classi.BuildControl}>
        <div>
            <div className={Classi.Label}>{props.label}</div> 
            <button className={Classi.Less} onClick={props.removed} disabled={props.disabled}>less</button>
            <button className={Classi.More} onClick={props.added}>plus</button>
        </div>
    </div>
)

export default ControllerBurger