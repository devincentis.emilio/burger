import React  from 'react';
import ButtonCss from './Button.module.css'
const button  = (props) => (
    <button onClick={props.clicked} className={[ButtonCss.Button, ButtonCss[props.BtnType]].join(' ')}>
        {props.children}
    </button>
)

export default button