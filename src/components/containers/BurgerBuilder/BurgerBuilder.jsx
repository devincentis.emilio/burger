import React, { Component } from 'react';
import BurgerView from './../../BurgerMaker/BurgerView';
import BurgerControlli from './../../BurgerMaker/BurgerControlli/BurgerControlli';
//import ingredients from './../../BurgerMaker/ingredients/Ingredients';
import Modal from './../../UI/Modal/Modal';
import Summary from './../../BurgerMaker/Summary/Summary';


const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.6
}

class BurgerBuilder extends Component {
    state = {
        ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0
        },
        totalPrice: 4,
        purchasable: false,
        purchasing: false
    }

    addIngredientHandler = type => {
        const oldCount = this.state.ingredients[type]
        const updatedCount = oldCount + 1
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice + priceAddition
        this.setState({ totalPrice: newPrice, ingredients: updatedIngredients })
        this.updatePurchasable(updatedIngredients)
    }

    removeIngredientHandler = type => {
        const oldCount = this.state.ingredients[type]
        if (oldCount > 0) {
            const updatedCount = oldCount - 1
            const updatedIngredients = {
                ...this.state.ingredients
            }
            updatedIngredients[type] = updatedCount;
            const priceSub = INGREDIENT_PRICES[type]
            const oldPrice = this.state.totalPrice
            const newPrice = oldPrice - priceSub
            this.setState({ totalPrice: newPrice, ingredients: updatedIngredients })
            this.updatePurchasable(updatedIngredients)
        }

    }
    updatePurchasable(ingredients) {
        const sum = Object.keys(ingredients).map(ingKey => ingredients[ingKey]).reduce((sum, el) => sum + el, 0)

        this.setState({ purchasable: sum > 0 })

    }
    purchaseHandler = () => {
        this.setState({ purchasing: true })
    }
    purchaseCancelHandler = () => this.setState({ purchasing: false })
    purchaseContinueHandler = () => alert('prezzo ' +  this.state.totalPrice.toFixed(2))

    render() {
        const disable = {
            ...this.state.ingredients
        }
        for (let key in disable) {
            disable[key] = disable[key] <= 0
        }

        return (
            <>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    <Summary 
                        ingredients={this.state.ingredients}
                        purchaseCanceled={this.purchaseCancelHandler}
                        purchaseContinued={this.purchaseContinueHandler}
                        price= {this.state.totalPrice}
                             />
                </Modal>
                <BurgerView ingredients={this.state.ingredients} />
                <BurgerControlli IngredientsAdded={this.addIngredientHandler}
                    IngredientsRemoved={this.removeIngredientHandler}
                    disabled={disable} price={this.state.totalPrice}
                    purchasable={this.state.purchasable}
                    ordered={this.purchaseHandler}
                />

            </>
        )
    }
}

export default BurgerBuilder
