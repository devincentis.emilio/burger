import React from 'react'
import layoutCss from './layout.module.css'
import Toolbar from './../Navigation/ToolBar';

const Layout = (props) => (
    <>
        <Toolbar/>
        <main className={layoutCss.Content}>
            {props.children}
        </main>
    </>
)

export default Layout